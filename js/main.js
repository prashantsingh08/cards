// Data to Show
var myDataBase = [
    {name:'Abhas Srivastava', email:'abhas@gmail.com', age:22},
    {name:'Akash Madhukar', email:'akki@gmail.com', age:22},
    {name:'Raman Agnihotri', email:'raman@gmail.com', age:21},
];


// Calls when execuated
(function Avatars(db) {
    var init = function() {
        generateList();
        enterUser();
    }
    
    // Generates Card
    var generateList = function() {
        var parent = document.querySelector('#parent-avatars');
        var template = '';

        for (var i = 0; i < db.length; i++) {
            template +='<div class="col-sm-4">';   
            template +=    '<div class="card">';
            template +=        '<div class="card-delete" data-card="'+ i +'">X</div>';
            template +=        '<div class="card-block">';
            template +=            '<h3 class="card-title">' + db[i].name + '</h3>';
            template +=                '<strong>Email</strong>:<span>' + db[i].email + '</span>';
            template +=            '</p>';
            template +=            '<p class="card-text">';
            template +=            '<p class="card-text">';
            template +=                '<strong>Age</strong>:<span>' + db[i].age + '</span>';
            template +=            '</p>';
            template +=        '</div>';
            template +=    '</div>';
            template +='</div>';
        }

        parent.innerHTML = '';
        parent.insertAdjacentHTML('afterbegin', template);
        deleteCard();
    }

    // Populating DataBase
    var enterUser = function() {

        function grabUser() {
            var name = document.querySelector('#user-name').value;
            var email = document.querySelector('#user-email').value;
            var age = document.querySelector('#user-age').value;

            var elements = [name, email, age];

            if(validateUser(elements)) {
                document.querySelector('#myForm').reset();
                db.push({name:name, email:email, age:age},);
                generateList();

            } else {
                document.querySelector('#error').style.display = 'block';

                setTimeout(function(){
                    document.querySelector('#error').style.display = 'none';
                }, 3000);
            }
        }
        
        document.querySelector('#myForm').addEventListener("submit", function(event){
            event.preventDefault();
            grabUser();
        })
    }

    // Input Validator
    var validateUser = function(elements) {
        for(var i = 0; i < elements.length; i++) {
            if(elements[i] == '') {
                return false;
            }
        }

        return true;
    }

    // Card delete
    var deleteCard = function() {
        var buttons = document.querySelectorAll('.card-delete');

        function deleteThis(element) {
            var obj = parseInt(element.getAttribute('data-card'));
            db.splice(obj,1);
            generateList();
        }

        for(var i = 0; i < buttons.length; i++){
            buttons[i].addEventListener('click', function(event) {
                deleteThis(this);
            })
        }
    }

    init();

}(myDataBase));